import './Content.css'
import PageToggle from '../PageToggle/PageToggle.jsx'
import React from "react"

var HelpersMNG=React.createClass({
    getInitialState: function(){
        return {
            totals:this.props.data.totals,
            currentPage:this.props.data.page,
            count:this.props.data.count
        }
    },
    componentDidMount: function () {
        this.setEvent();
    },
    componentWillReceiveProps: function(nextProps) {
        this.setState({
            totals:nextProps.data.totals,
            currentPage:nextProps.data.page,
            count:nextProps.data.count
        });
    },
    setEvent: function () {
    },
    selectPage: function (page) {
        this.props.callback.changePage({page:page,name:this.props.name});
    },
    render: function(){
        return (
            <div className="Content-task" id={this.props.name}>
                <div className="Content-taskTitle">{this.props.type}</div>
                <div className="Content-taskCon">
                {React.Children.map(this.props.children, function(element, index){
                        return (element)
                })}
                <PageToggle totals={this.state.totals} currentPage={this.state.currentPage} count={this.state.count} callbackPage={this.selectPage}></PageToggle>
                </div>
            </div>);
    }
});

export default HelpersMNG
