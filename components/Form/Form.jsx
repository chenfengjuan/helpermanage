import React from 'react'

class Form extends React.Component{
  constructor(props){
    super(props);
    this.state={}
    this._handleChange=this.handleChange.bind(this);
    this._requestData=this.requestData.bind(this);
    this._openDate=this.openDate.bind(this);
    this._emojiTo=this.emojiTo.bind(this);
  }
  emojiTo(arg){
      if((arg.indexOf("<span")>-1)&&(arg.indexOf("</span>")>-1)){
          var jsxDom=null;
          var str1=arg.split("></span>");
          $.each(str1,function (i, item) {
              if((item.indexOf("<span")>-1)&&(item.indexOf("class")>-1)){
                  var str2=item.split("<span");
                  if(str2[0].indexOf("class=")>-1){
                      var classN=str2[0].replace(" class=","").replace('"',"").replace('"',"");
                      if((jsxDom=null)&&(str2[1]=="")){
                          jsxDom=<span className={classN}></span>;
                      }else{
                          jsxDom=<span>{jsxDom}<span className={classN}></span>{str2[1]}</span>;
                      }
                  }else if(str2[1]!=""){
                      var classN=str2[1].replace(" class=","").replace('"',"").replace('"',"");
                      if((str2[0]=="")&&(jsxDom==null)){
                          jsxDom=<span className={classN}></span>;
                      }else{
                          jsxDom=<span>{jsxDom}{str2[0]}<span className={classN}></span></span>;
                      }
                  }else{
                      jsxDom=<span>{jsxDom}{str2[1]}</span>;
                  }
              }else if(item!=""){
                  jsxDom=<span>{jsxDom}{item}</span>;
              }
              });
          return jsxDom;
      }
      return arg;
  }
  openDate(e){
    $.jeDate(e.target,{
      insTrigger:false,
      isTime:true,
      format:'YYYY-MM-DD  hh:mm:ss',
      ishmsVal:false,
      maxDate:$.nowDate(0),
      zIndex:3000
    });
  }
  requestData(page){}
  componentWillReceiveProps(nextProps) {
      if(nextProps.data.isRefresh!=this.state.isRefresh){
          this._requestData(nextProps.data.page);
          this.state.isRefresh=nextProps.data.isRefresh;
      }
  }
  checkNull(arg){
      if(arg==null){
          return "";
      }else {
          return arg;
      }
  }
  handleChange(e){
      this.setState({
          [e.target.name]:e.target.value
      });
  }
  render(){
    return null;
  }
}

export default Form
