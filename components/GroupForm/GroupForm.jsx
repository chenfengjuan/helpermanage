import './GroupForm.css'
import React from 'react'
import Form from '../Form/Form.jsx'

class GroupForm extends Form{
    constructor(props){
        super(props);
        this.state={
            groupName:"",
            isRefresh:this.props.data.isRefresh
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._requestData(1);
    }
    getFormData(page){
        var formData=new Object();
        formData.groupName=this.state.groupName;
        formData.page=page!=undefined?page:1;
        formData.pageSize=20;
        formData.clerkID=this.props.data.userID;
        return formData;
    }
    requestData(page){
        var self=this;
        page=page==undefined?1:page;
        $.ajax({
            type: 'get',
            url: self.props.data.url+"HelperManage/group/getGroup",
            data: self.getFormData(page),
            dataType: 'json',
            success: function(data){
              if(data.status==200){
                data.count=data.data.total;
                data.totals=data.data.count;
                data.page=page;
                data.data.tbody=self.getTbody(data.data.groups);
                data.data.thead=self.setThead();
                data.data.tableCSS=self.setTableCss();
                self.props.callback.returnData({data:data,parent:"group"});
              }
            }
        });
    }
    setTableCss(){
      return "GroupForm-tableLayoutGroup";
    }
    setThead(){
      var thead=[{name:"群code",width:'30px'},
          {name:"群名字",width:'50px'},
          {name:"总人数",width:'10px'},
          {name:"地区",width:'14px'},
          {name:"省份",width:'14px'},
          {name:"城市",width:'14px'},
          {name:"部门",width:'14px'},
          {name:"群附属名字",width:'30px'},
          {name:"群助手渠道",width:'20px'},
          {name:"群别名",width:'20px'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    getTbody(data){
      var tr=data.map(function (item, i) {
          return <tr key={i}><td>{item.roomID}
              </td><td>{item.roomName}
              </td><td>{item.userCount}
              </td><td>{this.checkNull(item.area)}
              </td><td>{this.checkNull(item.province)}
              </td><td>{this.checkNull(item.city)}
              </td><td>{this.checkNull(item.department)}
              </td><td>{this.checkNull(item.groupAttr)}
              </td><td>{this.checkNull(item.channel)}
              </td><td>{this.checkNull(item.alias)}
              </td></tr>
      }.bind(this));
      return tr;
    }
    searchData(){
        this._requestData(1);
    }
    render(){
        return <div className="GroupForm-formCss">
            <input placeholder="群名" name="groupName" value={this.state.groupName} className="GroupForm-inputCSS GroupForm-helpersCSS2" onChange={this._handleChange}></input>
            <button className="GroupForm-buttonCSS" onClick={this._searchData}>搜索</button>
        </div>
    }
}

export default GroupForm
