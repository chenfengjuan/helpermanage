import React from 'react'
import './H5Enter.css'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class H5Enter extends Form{
    constructor(props){
        super(props);
        var selectData=new Array();
        $.ajax({
            type: 'get',
            url: this.props.data.helperData.url,
            data:this.props.data.helperData.data,
            async: false,
            dataType: 'json',
            success: function(data){
                $.each(data.data,function (i, item) {
                    selectData.push({
                      value:item.robotid,
                      name:item.robotname
                    });
                });
            }
        });

        this.state={
            helperid:selectData.length>0?selectData[0].value:"",
            msg:"",
            name:"",
            status:"",
            isRefresh:this.props.data.isRefresh
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._changeEnter=this.changeEnter.bind(this);
        this._openOverflow=this.openOverflow.bind(this);
        this._handleChange=this.handleChange.bind(this);
        this._requestData(1);
    }
    handleChange(e){
      if(e.target.name=="status"||e.target.name=="helperid"){
          this.state[e.target.name]=e.target.value;
          this._requestData(1);
      }else{
        this.setState({
            [e.target.name]:e.target.value
        });
      }

    }
    changeEnter(e){
        var self=this;
        var text=e.target.value;
        var data=new Object();
        data.entergroupstatus=e.target.value;
        data.id=e.target.name;
        if (confirm("确定要修改入群状态吗？")) {
            //发送保存修改的请求
            $.ajax({
                type: "post",
                url: self.props.data.url+"HelperManage/message/modifyMessage2",
                data: data,
                dataType: "json",
                success: function (data) {
                    self._requestData(self.props.data.page);
                }
            });
        }else{
            if(text=="手动邀请"){
                $(e.target).val("未入群");
            }else{
                $(e.target).val("手动邀请");
            }
        }
    }
    openOverflow(e){
        e.persist();
        var target;
        if($(e.target).prop("tagName")!="TR"){
            target=$(e.target).parent("tr");
        }else{
            target=$(e.target);
        }
        if(target.hasClass("H5Enter-textOverflow")){
            target.removeClass("H5Enter-textOverflow");
        }else{
            target.addClass("H5Enter-textOverflow");
        }
    }
    getFormData(page){
      var formData=new Object();
        formData.robotID=this.state.helperid;
        formData.code=this.state.msg;
        formData.username=this.state.name;
        formData.enterRoomStatus=this.state.status;
        formData.page=page==undefined?1:page;
        formData.pageSize=20;
        formData.roomName="";
        formData.urlStatus="";
        return formData;
    }
    requestData(page){
        var self=this;
        page=page==undefined?1:page;
        $.ajax({
            type: 'post',
            url: self.props.data.url+'HelperManage/message/selectMessage2',
            data: self.getFormData(page),
            dataType: 'json',
            success: function(data){
              if(data.status==200){
                data.count=data.data.total;
                data.totals=data.data.count;
                data.page=page;
                data.data.tbody=self.getTbody(data.data.messages);
                data.data.thead=self.setThead();
                data.data.tableCSS=self.setTableCss();
                self.props.callback.returnData({data:data,parent:"h5enter"});
              }
            }
        });
    }
    setThead(){
      var thead=[{name:"用户昵称",width:'80px'},
          {name:"微信群名",width:'120px'},
          {name:"短信邀请码",width:'60px'},
          {name:"入群状态",width:'60px'},
          {name:"H5状态",width:'90px'},
          {name:"H5信息",width:'146px'},
          {name:"提醒消息",width:'90px'},
          {name:"群匹配状态",width:'60px'},
          {name:"H5提交时间",width:'60px'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    setTableCss(){
      return "H5Enter-tableLayoutH5";
    }
    getTbody(data){
      var tr=data.map(function (item, i) {
          var str="";
          if((item.city==null)&&(item.hospitalKeyword==null)&&(item.edc==null)){
              str="";
          }else{
              str=this.checkNull(item.city)+"+"+this.checkNull(item.hospitalKeyword)+"+"+this.checkNull(item.edc);
          }
          var enter=this.checkNull(item.enterGroupStatus);
          var tdCSS="";
          if((enter=="未入群")||(enter=="手动邀请")){
              tdCSS="H5Enter-tdCSS";
              var enter2="";
              if(enter=="未入群"){
                  enter2="手动邀请";
              }else{
                  enter2="未入群";
              }
              enter=<select className='H5Enter-selectCSS H5Enter-enterSelect' name={item.id} value={enter} onChange={this._changeEnter}>
                  <option value={enter}>{enter}</option>
                  <option value={enter2}>{enter2}</option>
              </select>
          }
          return <tr key={i} onDoubleClick={this._openOverflow} className='H5Enter-textOverflow'>
              <td>{this._emojiTo(this.checkNull(GB2312UnicodeConverter.ToGB2312(item.userName)))}</td>
              <td>{this.checkNull(item.nowRoomName)}</td>
              <td>{this.checkNull(item.phoneCode)}</td>
              <td className={tdCSS}>{enter}</td>
              <td>{this.checkNull(item.h5Status)}</td>
              <td>{str}</td>
              <td>{this.checkNull(item.remindMessage)}</td>
              <td>{this.checkNull(item.matchStatus)}</td>
              <td>{this.checkNull(item.formTime)}</td>
          </tr>
      }.bind(this));
      return tr;
    }
    searchData(){
        this._requestData(1);
    }
    render(){
        return <div className="H5Enter-formCss">
            <ReactSelect placeholder="小助手" dataSearch="true" data={this.props.data.helperData}
                         className="H5Enter-helper" name="helperid"
                         onChange={this._handleChange} >
            </ReactSelect>
            <input placeholder="短信邀请码" name="msg" value={this.state.msg} className="H5Enter-inputCSS H5Enter-helpersCSS1" onChange={this._handleChange}></input>
            <input placeholder="用户昵称" name="name" value={this.state.name} className="H5Enter-inputCSS H5Enter-helpersCSS1" onChange={this._handleChange}></input>
            <select className="H5Enter-selectCSS" name="status" onChange={this._handleChange} value={this.state.status}>
                <option value="">All</option>
                <option value="未入群">未入群</option>
                <option value="手动邀请">手动邀请</option>
                <option value="已邀请">已邀请</option>
            </select>
            <button className="H5Enter-buttonCSS" onClick={this._searchData}>搜索</button>
        </div>
    }
}

export default H5Enter
