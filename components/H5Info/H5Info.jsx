import React from 'react'
import './H5Info.css'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class H5Info extends Form{
    constructor(props){
        super(props);

        this.state={
          h5Status:"null",
          province:"null",
          city:"null",
          hospital:"null",
          match:"null",
          phone:"",
          // name:"",
          helperid:"null",
          provinces: [],
          citys: [],
          hospitals: []
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._openOverflow=this.openOverflow.bind(this);
        this._handleChange=this.handleChange.bind(this);
        // this._requestData(1);
    }
    componentDidMount(){
      this.getProvinces();
    }
    getProvinces(){
      var self=this;
      $.ajax({
          type: 'get',
          url: self.props.data.url+'HelperManage/dicdata/provinces',
          success: function(data){
            self.setState({
                provinces:data,
                province:'null',
                city:'null',
                citys:[],
                hospital:'null',
                hospitals:[]
              });
          },
          error: function(data){
            console.log(data);
          }
      });
    }
    getCitys(provinceName){
      var self=this;
      $.ajax({
          type: 'get',
          url: self.props.data.url+'HelperManage/dicdata/province/'+provinceName+'/citys',
          success: function(data){
              self.setState({
                citys:data,
                city:'null',
                hospital:'null',
                hospitals:[]
              });
              self.state.city="null";
              self.state.hospital="null";
              self._requestData(1);
          }
      });
    }
    getHospitals(cityName){
      var self=this;
      $.ajax({
          type: 'get',
          url: self.props.data.url+'HelperManage/dicdata/city/'+cityName+'/hospitals?type=1',
          success: function(data){
              self.setState({
                hospitals:data,
                hospital:'null'
              });
              self.state.hospital="null";
              self._requestData(1);
          }
      });
    }
    handleChange(e){
        if(e.target.name=="h5Status"||e.target.name=="helperid"||e.target.name=="province"||e.target.name=="city"||e.target.name=="match"||e.target.name=="hospital"){
            this.state[e.target.name]=e.target.value;
            if(e.target.name=="province"){
              this.getCitys(e.target.value);
            }else if(e.target.name=="city"){
              this.getHospitals(e.target.value);
            }else{
              this._requestData(1);
            }
        }else{
          if(e.target.name=='phone'){
            this.checkPhone(e);
          }
          this.setState({
              [e.target.name]:e.target.value
          });
        }
    }
    checkPhone(e){
      e.target.value=e.target.value.replace(/\D/g,'');
      if(e.target.value.length>11){
        e.target.value=e.target.value.substr(0,11);
      }
    }
    openOverflow(e){
        e.persist();
        var target;
        if($(e.target).prop("tagName")!="TR"){
            target=$(e.target).parent("tr");
        }else{
            target=$(e.target);
        }
        if(target.hasClass("H5Info-textOverflow")){
            target.removeClass("H5Info-textOverflow");
        }else{
            target.addClass("H5Info-textOverflow");
        }
    }
    getFormData(page){
        var formData=new Object();
          formData.activeStatus=(this.state.h5Status==""||this.state.h5Status=="All"||this.state.h5Status=="null")?null:this.state.h5Status;
          formData.province=(this.state.province==""||this.state.province=="All"||this.state.province=="null")?null:this.state.province;
          formData.city=(this.state.city==""||this.state.city=="All"||this.state.city=="null")?null:this.state.city;
          formData.hospital=(this.state.hospital==""||this.state.hospital=="All"||this.state.hospital=="null")?null:this.state.hospital;
          formData.matchStatus=(this.state.match==""||this.state.match=="All"||this.state.match=="null")?null:this.state.match;
          // formData.userName=this.state.name;
          formData.phoneNumber=this.state.phone;
          formData.robotId=this.state.helperid=="null"?null:this.state.helperid;
          formData.page=page;
          formData.size=20;
          // console.log(formData);
        return formData;
    }
    requestData(page){
        var self=this;
        page=page==undefined?1:page;
        if(this.state.phone.length==4||this.state.phone.length==11||this.state.phone.length==0){
          $.ajax({
              type: 'post',
              url: self.props.data.url+'HelperManage/card/userinfos',
              data:self.getFormData(page),
              success: function(data){
                if(data.resultCode==100){
                  let returnData={
                    count:data.pageInfo.total,
                    totals:data.pageInfo.pages,
                    page:page,
                    data:{
                      tbody:self.getTbody(data.pageInfo.list),
                      thead:self.setThead(),
                      tableCSS:self.setTableCss()
                    }
                  };
                    self.props.callback.returnData({data:returnData,parent:"h5Info"});
                }
              }
          });
        }else{
          alert("手机号请输入11位或末4位");
        }

    }
    setThead(){
      var thead=[
        // {name:"用户昵称",width:'80px'},
          {name:"H5状态",width:'80px'},
          {name:"手机号",width:'90px'},
          {name:"省份",width:'90px'},
          {name:"城市",width:'100px'},
          {name:"医院",width:'146px'},
          {name:"预产期信息",width:'90px'},
          {name:"群匹配状态",width:'80px'},
          {name:"小助手",width:'100px'},
          {name:"微信群名",width:'150px'},
          {name:"H5提交时间",width:'130px'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    setTableCss(){
      return "H5Info-tableLayoutH5";
    }
    getTbody(data){
      var tr=data.map(function (item, i) {
          return <tr key={i} onDoubleClick={this._openOverflow} className='H5Info-textOverflow'>
              {/* <td></td> */}
              <td>{this.checkNull(this.getH5status(item.activeStatus))}</td>
              <td>{this.checkNull(item.phoneNumber)}</td>
              <td>{this.checkNull(item.province)}</td>
              <td>{this.checkNull(item.city)}</td>
              <td>{this.checkNull(item.hospital)}</td>
              <td>{this.checkNull(item.edc)}</td>
              <td>{this.checkNull(this.getMatchStatus(item.matchStatus))}</td>
              <td>{this.checkNull(item.robotName)}</td>
              <td>{this.checkNull(item.roomName)}</td>
              <td>{this.checkNull(item.formTime)}</td>
          </tr>
      }.bind(this));
      return tr;
    }
    getMatchStatus(id){
      switch(id){
        case 0: return "未匹配";
        case 1: return "已匹配";
        case 2: return "匹配到城市群";
        case 3: return "匹配到全国群";
        default: return "-";
      }
    }
    getH5status(id){
      switch(id){
        case 0: return "未激活卡券";
        case 1: return "已激活卡券";
        default: return "-";
      }
    }
    searchData(){
        this._requestData(1);
    }
    getOptionCSS(arg){
      if(arg==""){
        return ""
      }else{
        return "H5Info-disabledOption"
      }
    }
    render(){
      var incloudData=[{name:"All",value:"null"}];
        return <div className="H5Info-formCss">
          <div className="H5Info-form">
          <select className="H5Info-selectCSS" name="h5Status" onChange={this._handleChange} value={this.state.h5Status}>
              <option value={""} disabled className={this.getOptionCSS(this.state.h5Status)}>H5状态</option>
              <option value={"All"} >All</option>
              <option value={0} >未激活卡券</option>
              <option value={1} >已激活卡券</option>
          </select>
          <select className="H5Info-selectCSS" name="province" onChange={this._handleChange} value={this.state.province}>
              <option value="" disabled className={this.getOptionCSS(this.state.province)}>省</option>
              <option value='All' >All</option>
              {this.state.provinces.map(function(item,i){
                return (<option key={i} value={item} >{item}</option>)
              })}
          </select>
          <select className="H5Info-selectCSS" name="city" onChange={this._handleChange} value={this.state.city}>
            <option value="" disabled className={this.getOptionCSS(this.state.city)}>城市</option>
              <option value='All' >All</option>
              {this.state.citys.map(function(item,i){
                return (<option key={i} value={item} >{item}</option>)
              })}
          </select>
          <select className="H5Info-selectCSS" name="hospital" onChange={this._handleChange} value={this.state.hospital}>
            <option value="" disabled className={this.getOptionCSS(this.state.hospital)}>医院</option>
              <option value='All' >All</option>
              {this.state.hospitals.map(function(item,i){
                return (<option key={i} value={item} >{item}</option>)
              })}
          </select>
          <select className="H5Info-selectCSS" name="match" onChange={this._handleChange} value={this.state.match}>
              <option value={""} disabled className={this.getOptionCSS(this.state.match)}>群匹配</option>
              <option value={'All'} >All</option>
              <option value={0} >未匹配</option>
              <option value={1} >已匹配</option>
              <option value={2} >匹配到城市群</option>
              <option value={3} >匹配到全国群</option>
          </select>
        </div>
        <div className="H5Info-form">
          <input placeholder="手机号/末4位" name="phone" value={this.state.phone} className="H5Info-inputCSS H5Info-helpersCSS1" onChange={this._handleChange}></input>
          {/* <input placeholder="用户昵称" name="name" value={this.state.name} className="H5Info-inputCSS H5Info-helpersCSS1" onChange={this._handleChange}></input> */}
            <ReactSelect placeholder="小助手" dataSearch="true" data={this.props.data.helperData}
                         className="H5Info-helper" name="helperid" incloudData={incloudData}
                         onChange={this._handleChange} >
            </ReactSelect>
            <button className="H5Info-buttonCSS" onClick={this._searchData}>搜索</button>
          </div>
        </div>
    }
}

export default H5Info
