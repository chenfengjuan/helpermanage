import React from 'react'
import './HelperInfo.css'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class HelperInfo extends Form{
    constructor(props){
        super(props);
        var selectData=new Array();
        $.ajax({
            type: 'get',
            url: this.props.data.helperData.url,
            data:this.props.data.helperData.data,
            async: false,
            dataType: 'json',
            success: function(data){
                $.each(data.data,function (i, item) {
                    selectData.push({
                      value:item.robotid,
                      name:item.robotname
                    });
                });
            }
        });

        this.state={
            helperid:"null",
            status:"",
            userMatch:"",
            groupStatus:"",
            name:"",
            phone:"",
            isRefresh:this.props.data.isRefresh
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._openOverflow=this.openOverflow.bind(this);
        this._getUserInfo=this.getUserInfo.bind(this);
        // this._changeEnter=this.changeEnter.bind(this);
        this._handleChange=this.handleChange.bind(this);
        this._phoneChange=this.phoneChange.bind(this);
    }
    phoneChange(e){
      this.checkPhone(e);
    }
    // changeEnter(e){
    //     var self=this;
    //     var text=e.target.value;
    //     var data=new Object();
    //     data.entergroupstatus=e.target.value;
    //     data.id=e.target.name;
    //     if (confirm("确定要修改入群状态吗？")) {
    //         //发送保存修改的请求
    //         $.ajax({
    //             type: "post",
    //             url: self.props.data.url+"",
    //             data: data,
    //             dataType: "json",
    //             success: function (data) {
    //                 self._requestData(self.props.data.page);
    //             }
    //         });
    //     }else{
    //         if(text=="手动邀请"){
    //             $(e.target).val("未入群");
    //         }else{
    //             $(e.target).val("手动邀请");
    //         }
    //     }
    // }
    componentDidMount(){
      this._requestData(1);
    }
    handleChange(e){
      if(e.target.name=="status"||e.target.name=="helperid"||e.target.name=="userMatch"||e.target.name=="groupStatus"){
          this.state[e.target.name]=e.target.value;
          this._requestData(1);
      }else{
        if(e.target.name=='phone'){
          this.checkPhone(e);
        }
        this.setState({
            [e.target.name]:e.target.value
        });
      }
    }
    checkPhone(e){
      e.target.value=e.target.value.replace(/\D/g,'');
      if(e.target.value.length>11){
        e.target.value=e.target.value.substr(0,11);
      }
    }
    openOverflow(e){
        e.persist();
        var target;
        if($(e.target).prop("tagName")!="TR"){
            target=$(e.target).parent("tr");
        }else{
            target=$(e.target);
        }
        if(target.hasClass("HelperInfo-textOverflow")){
            target.removeClass("HelperInfo-textOverflow");
        }else{
            target.addClass("HelperInfo-textOverflow");
        }
    }
    getFormData(page){
      var formData=new Object();
      formData.enterGroupStatus=(this.state.status==""||this.state.status=="All")?null:this.state.status;
      formData.matchStatus=(this.state.groupStatus==""||this.state.groupStatus=="All")?null:this.state.groupStatus;
      formData.userMatchStatus=(this.state.userMatch==""||this.state.userMatch=="All")?null:this.state.userMatch;
      formData.phoneNumber=this.state.phone;
      formData.robotId=this.state.helperid=="null"?null:this.state.helperid;
      formData.userName=this.state.name;
      formData.page=page;
      formData.size=20;
      return formData;
    }
    requestData(page){
        var self=this;
        page=page==undefined?1:page;
        if(this.state.phone.length==4||this.state.phone.length==11||this.state.phone.length==0){
          $.ajax({
              type: 'post',
              url: self.props.data.url+'HelperManage/card/userstatus',
              data: self.getFormData(page),
              success: function(data){
                if(data.resultCode==100){
                  let returnData={
                    count:data.pageInfo.total,
                    totals:data.pageInfo.pages,
                    page:page,
                    data:{
                      tbody:self.getTbody(data.pageInfo.list),
                      thead:self.setThead(),
                      tableCSS:self.setTableCss()
                    }
                  };
                  self.props.callback.returnData({data:returnData,parent:"helperInfo"});
                }
              }
          });
        }else{
            alert("手机号请输入11位或末4位");
        }
    }
    setThead(){
      var thead=[
        {name:"用户昵称",width:'80px'},
          {name:"入群状态",width:'80px'},
          {name:"手机号",width:'90px'},
          {name:"省份",width:'90px'},
          {name:"城市",width:'100px'},
          {name:"医院",width:'146px'},
          {name:"预产期信息",width:'90px'},
          {name:"提醒消息",width:'120px'},
          {name:"用户匹配状态",width:'80px'},
          {name:"群匹配状态",width:'80px'},
          {name:"微信群名",width:'140px'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    setTableCss(){
      return "HelperInfo-tableLayoutH5";
    }
    getUserInfo(e){
      var self=this;
      e.persist();
      if(e.which==13){
        if(e.target.value.length==4||e.target.value.length==11){
          $.ajax({
            url:self.props.data.url+"HelperManage/card/userstatus/matchuserinfo",
            type:"post",
            data:{
              id:e.target.name,
              phoneNumber:e.target.value
            },
            success:function(data){
              if(data.resultCode==100){
                self._requestData(self.props.data.page);
              }else if(data.resultCode==300){
                alert(data.detailDescription);
              }else{
                alert("操作异常");
              }
            }
          });
        }else{
          alert("手机号请输入11位或末4位");
        }
      }
    }
    getTbody(data){
      var tr=data.map(function (item, i) {
        var phone=item.phoneNumber;
        if(phone==""||phone==null){
          phone=<input className="HelperInfo-phoneInput" placeholder="手机号/末4位" onChange={this._phoneChange} name={item.id} onKeyDown={this._getUserInfo}></input>
        }
        // var enter=this.getEnterGroupStatus(this.checkNull(item.enterGroupStatus));
        // var tdCSS="";
        // if((enter=="未入群")||(enter=="手动邀请")){
        //     tdCSS="HelperInfo-tdCSS";
        //     var enter2="";
        //     if(enter=="未入群"){
        //         enter2="手动邀请";
        //     }else{
        //         enter2="未入群";
        //     }
        //     enter=<select className='HelperInfo-selectCSS HelperInfo-enterSelect' name={item.id} value={enter} onChange={this._changeEnter}>
        //         <option value={enter}>{enter}</option>
        //         <option value={enter2}>{enter2}</option>
        //     </select>
        // }
          return <tr key={i} onDoubleClick={this._openOverflow} className='HelperInfo-textOverflow'>
              <td>{this._emojiTo(this.checkNull(GB2312UnicodeConverter.ToGB2312(item.userName)))}
              </td><td>{this.getEnterGroupStatus(this.checkNull(item.enterGroupStatus))}
              </td><td name={item.id}>{phone}
              </td><td>{this.checkNull(item.province)}
              </td><td>{this.checkNull(item.city)}
              </td><td>{this.checkNull(item.hospital)}
              </td><td>{this.checkNull(item.edc)}
              </td><td>{this.checkNull(item.remindMessage)}
              </td><td>{this.getUserMatch(this.checkNull(item.userMatchStatus))}
              </td><td>{this.getMatchStatus(this.checkNull(item.matchStatus))}
              </td><td>{this.checkNull(item.roomName)}
              </td></tr>
      }.bind(this));
      return tr;
    }
    getEnterGroupStatus(id){
      switch(id){
        case 0: return "未入群";
        case 1: return "已邀请";
        case 2: return "手动邀请";
        default: return "-";
      }
    }
    getMatchStatus(id){
      switch(id){
        case 0: return "未匹配";
        case 1: return "已匹配";
        case 2: return "匹配到城市群";
        case 3: return "匹配到全国群";
        default: return "-";
      }
    }
    getUserMatch(id){
      switch(id){
        case 1: return "确认好友";
        case 2: return "匹配成功";
        default: return "-";
      }
    }
    searchData(){
        this._requestData(1);
    }
    render(){
      var incloudData=[{name:"All",value:"null"}];
        return <div className="HelperInfo-formCss">
          <div className="HelperInfo-form">
            <ReactSelect placeholder="小助手" dataSearch="true" data={this.props.data.helperData}
                         className="HelperInfo-helper" name="helperid" defaultValue={this.state.helperid} incloudData={incloudData}
                         onChange={this._handleChange} >
            </ReactSelect>
            <select className="HelperInfo-selectCSS" placeholder="入群状态" name="status" onChange={this._handleChange} value={this.state.status}>
                <option value={null} disabled className="HelperInfo-disabledOption">入群状态</option>
                <option value="All" >All</option>
                <option value={0}>未入群</option>
                <option value={1}>已邀请</option>
                <option value={2}>手动邀请</option>
                {/* <option value={3}>已邀请</option> */}
            </select>
            <select className="HelperInfo-selectCSS" placeholder="用户匹配" name="userMatch" onChange={this._handleChange} value={this.state.userMatch}>
                <option value={null} disabled className="HelperInfo-disabledOption">用户匹配</option>
                <option value="All">All</option>
                <option value={1}>确认好友</option>
                <option value={2}>匹配成功</option>
            </select>
            <select className="HelperInfo-selectCSS" placeholder="群匹配" name="groupStatus" onChange={this._handleChange} value={this.state.groupStatus}>
                <option value={null} disabled className="HelperInfo-disabledOption">群匹配</option>
                <option value="All" >All</option>
                <option value={0}>未匹配</option>
                <option value={1}>已匹配</option>
                <option value={2}>匹配到城市群</option>
                <option value={3}>匹配到全国群</option>
            </select>
          </div>
          <div className="HelperInfo-form">
            <input placeholder="用户昵称" name="name" value={this.state.name} className="HelperInfo-inputCSS HelperInfo-helpersCSS1" onChange={this._handleChange}></input>
            <input placeholder="手机号/末4位" name="phone" value={this.state.phone} className="HelperInfo-inputCSS HelperInfo-helpersCSS1" onChange={this._handleChange}></input>
            <button className="HelperInfo-buttonCSS" onClick={this._searchData}>搜索</button>
          </div>
        </div>
    }
}

export default HelperInfo
