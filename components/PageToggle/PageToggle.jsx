import './PageToggle.css'
import React from "react"

var PageToggle=React.createClass({
    handleClick: function (e) {
        var page=$(e.target).attr("href").replace("#","");
        this.props.callbackPage(page);
    },
    getButtonCss: function (v) {
        return v==this.props.currentPage?"pt-pageButton pt-selected":"pt-pageButton";
    },
    getPageArray: function (count,currentIndex) {
        var startIndex=1;
        var endIndex=1;
        var pageArray=new Array();
        if(count>7){
            startIndex=currentIndex-3<1?1:currentIndex-3;
            endIndex=currentIndex+3>count?count:currentIndex+3;
            if(endIndex-startIndex<6){
                if(startIndex==1){
                    endIndex=startIndex+6;
                }else{
                    startIndex=endIndex-6;
                }
            }
        }else{
            startIndex=1;
            endIndex=count;
        }
        for(var i=startIndex;i<=endIndex;i++){
            pageArray.push(i);
        }
        return pageArray;
    },
    getToggleCss: function () {
        var count=parseInt(this.props.totals);
        return count>1?"pt-pagecom":"pt-pagecom pt-none-box"
    },
    render: function () {
        var that=this;
        var count=parseInt(this.props.totals);
        var currentIndex=parseInt(this.props.currentPage);
        var pageArray=this.getPageArray(count,currentIndex);
        return (
            <div className={this.getToggleCss()}>
                <div className="pt-pageInfo">
                    <label className="pt-pageCurrentIndex">{this.props.currentPage}</label>
                    <label>/</label>
                    <label className="pt-pageTotals">{this.props.totals}</label>
                    <label className="pt-pageTotals">    共{this.props.count}条纪录</label>
                </div>
                <div className="pt-pageToggle">
                    <a className="pt-pageButton" onClick={this.handleClick} href="#1">首页</a>
                    <a className="pt-pageButton" onClick={this.handleClick} href={'#'+(parseInt(this.props.currentPage)-1>0?parseInt(this.props.currentPage)-1:1)}>上一页</a>
                {
                    pageArray.map(function (v, i) {
                        return(<a key={i} className={that.getButtonCss(v)} onClick={that.handleClick} href={'#'+v}>{v}</a>);
                    })
                }
                    <a className="pt-pageButton" onClick={this.handleClick} href={'#'+(parseInt(this.props.currentPage)+1>parseInt(this.props.totals)?parseInt(this.props.totals):parseInt(this.props.currentPage)+1)}>下一页</a>
                    <a className="pt-pageButton" onClick={this.handleClick} href={'#'+this.props.totals}>尾页</a>
                </div>
            </div>
        );
    }
});

export default PageToggle
