import React from "react"
import {TabControl,Tab} from '../TabControl/TabControl.jsx'
import Content from '../Content/Content.jsx'
import HelperForm from '../HelperForm/HelperForm.jsx'
import GroupForm from '../GroupForm/GroupForm.jsx'
import Table from '../Table/Table.jsx'
import KeyForm from '../KeyForm/KeyForm.jsx'
import InForm from '../InForm/InForm.jsx'
import EnterForm from '../UpEnterForm/EnterForm.jsx'
import H5Enter from '../H5Enter/H5Enter.jsx'
import H5Info from '../H5Info/H5Info.jsx'
import HelperInfo from '../HelperInfo/HelperInfo.jsx'
var SideTab = React.createClass({
    getInitialState: function(){
        return {
            helper:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:true,
                url:this.props.data.url,
                userID: this.props.data.userID,
            },
            group:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID
            },
            upenter:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            h5enter:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            keyword:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            atgroup:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            helperInfo:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                // url:'http://192.168.0.143:8080/',
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            h5Info:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                // url:'http://192.168.0.143:8080/',
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            }
        }
    },
    changePage: function (args) {
        this.setState(function () {
            this.state[args.name].page=args.page;
            this.state[args.name].isRefresh=!this.state[args.name].isRefresh;
            return this.state;
        });
    },
    getData: function (args) {
        this.setState(function () {
            this.state[args.parent].data=args.data.data;
            this.state[args.parent].totals=args.data.totals;
            this.state[args.parent].page=args.data.page;
            this.state[args.parent].count=args.data.count;
            return this.state;
        });
    },
    render:function(){
        return (
            <TabControl>
                    <Tab name="小助手管理">
                        <Content name="helper" type="小助手管理" data={this.state.helper}>
                            <HelperForm name="helper" data={this.state.helper} callback={{returnData:this.getData}}></HelperForm>
                            <Table name="helper" data={this.state.helper} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="群管理">
                        <Content type="群管理" name="group" data={this.state.group} callback={{changePage:this.changePage}}>
                            <GroupForm name="group" data={this.state.group} callback={{returnData:this.getData}}></GroupForm>
                            <Table name="group" data={this.state.group} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="入群管理" type="parent">
                      <Tab name="H5入群" type="child">
                          <Content type="H5入群" name="h5enter" data={this.state.h5enter} callback={{changePage:this.changePage}}>
                              <H5Enter name="h5enter" data={this.state.h5enter} callback={{returnData:this.getData}}></H5Enter>
                              <Table name="h5enter" data={this.state.h5enter} ></Table>
                          </Content>
                      </Tab>
                      <Tab name="上行关键字" type="child">
                          <Content type="上行关键字" name="upenter" data={this.state.upenter} callback={{changePage:this.changePage}}>
                              <EnterForm name="upenter" data={this.state.upenter} callback={{returnData:this.getData}}></EnterForm>
                              <Table name="upenter" data={this.state.upenter} ></Table>
                          </Content>
                      </Tab>
                  </Tab>
                  <Tab name="关键字管理">
                        <Content type="关键字管理" name="keyword" data={this.state.keyword} callback={{changePage:this.changePage}}>
                            <KeyForm name="keyword" data={this.state.keyword} callback={{returnData:this.getData}}></KeyForm>
                            <Table name="keyword" data={this.state.keyword} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="小助手所在群">
                        <Content type="小助手所在群" name="atgroup" data={this.state.atgroup} callback={{changePage:this.changePage}}>
                            <InForm name="keyword" data={this.state.atgroup} callback={{returnData:this.getData}}></InForm>
                            <Table name="atgroup" data={this.state.atgroup} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="卡券入群" type="parent">
                        <Tab name="H5信息" type="child">
                            <Content type="H5信息" name="h5Info" data={this.state.h5Info} callback={{changePage:this.changePage}}>
                                <H5Info name="h5Info" data={this.state.h5Info} callback={{returnData:this.getData}}></H5Info>
                                <Table name="h5Info" data={this.state.h5Info} ></Table>
                            </Content>
                        </Tab>
                        <Tab name="小助手信息" type="child">
                            <Content type="小助手信息" name="helperInfo" data={this.state.helperInfo} callback={{changePage:this.changePage}}>
                                <HelperInfo name="helperInfo" data={this.state.helperInfo} callback={{returnData:this.getData}}></HelperInfo>
                                <Table name="helperInfo" data={this.state.helperInfo} ></Table>
                            </Content>
                        </Tab>
                    </Tab>
            </TabControl>
        );
    }
});

export default SideTab
