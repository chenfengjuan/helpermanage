import React from 'react'
import ReactDOM from 'react-dom'
import './WaitAnimation.css'
class WaitAnimation extends React.Component{
    constructor(props){
        super(props);
        this.state={
            imgurl:null,
            requestCount:0
        }
        this._cancel=this.cancel.bind(this);
        this._requestImg=this.requestImg.bind(this);
        this._requestImg();
    }
    requestImg(){
        var self=this;
        $.ajax({
            type: this.props.method,
            async:true,
            data:this.props.data,
            url: self.props.url,
            success: function (data) {
              if(data.data==null){
                  self.setState({
                      imgurl:data.data,
                      requestCount:self.state.requestCount+1
                  });
                  if(self.state.requestCount<3){
                      setTimeout(self._requestImg,3000);
                  }
              }else{
                  self.setState({
                      imgurl:data.data,
                      requestCount:0
                  });
              }
            },
            error: function(e){
              self.setState({
                  requestCount:self.state.requestCount+1,
                  imgurl:null
              });
            }
        });
    }
    cancel(){
        this.props.callback();
        ReactDOM.unmountComponentAtNode($('.'+this.props.container)[0]);
        $('.'+this.props.container).remove();
    }
    getloaderCSS(){
        if((this.state.imgurl==null)&&(this.state.requestCount<3)){
            return "ball-spin-fade-loader";
        }else{
            return ""
        }
    }
    getrelodeCSS(){
        if((this.state.requestCount>0)&&(this.state.requestCount<3)){
            return "WaitAnimation-red";
        }else{
            return "WaitAnimation-red WaitAnimation-none-box";
        }
    }
    getloseCSS(){
        return this.state.requestCount==3?"WaitAnimation-red":"WaitAnimation-red WaitAnimation-none-box";
    }
    getimgCSS(){
        return this.state.imgurl!=null?"WaitAnimation-addImg":"";
    }
    render(){
        return <div className="WaitAnimation-task" id={this.props.name}>
                <div className="WaitAnimation-taskTitle">
                  <span onClick={this._cancel} className="WaitAnimation-parentTitle">{this.props.parent}</span>
                  <span>&nbsp;&gt;&nbsp;</span>
                  <span>{this.props.type}</span></div>
                <div className="WaitAnimation-taskCon">
                    <div className="WaitAnimation-backBtn">{this.props.name}</div>
                    <div className={this.getrelodeCSS()}>正在重新加载，请稍后...</div>
                    <div className={this.getloseCSS()}>加载失败，请返回重试</div>
                    <div id="addCode" className="WaitAnimation-loader">
                        <div className={this.getloaderCSS()}>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <img id="addImg" className={this.getimgCSS()} src={this.state.imgurl} />
                    </div>
                    <button className="WaitAnimation-buttonCSS WaitAnimation-backBtn" onClick={this._cancel}>返回</button>
                </div>
            </div>
    }
}

export default WaitAnimation
