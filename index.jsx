import React from "react"
import ReactDOM from "react-dom"
import WebHead from "./components/WebHead/WebHead.jsx"
import SideTab from "./components/SideTab/SideTab.jsx"
class App extends React.Component{
    constructor(props){
        super(props);
        // var server="http://jbb.gemii.cc/";
        var server="http://wyeth.gemii.cc/";
        var login=this.isLogin(server);
        this.state={
            isLogin:true,
            server:server,
            helperData:{url:server+"HelperManage/status/getRobotByUid",data:{id:login.userID}},
            userID:login.userID
        }
        this._initData=this.initData.bind(this);
    }
    componentDidMount(){
        this.setLayout();
        $(window).resize(this.setLayout);
    }
    setLayout(){
        var bodyW=$(document.body).outerWidth();
        var bodyH=$(window).outerHeight();
        var minH=parseInt($('html').css('minHeight'));
        if(bodyH<=minH){
            bodyH=minH;
        }
        var headerH=$(".WebHead-headerContainer").outerHeight();
        var sideW=$(".TabControl-sideContainer").outerWidth();
        $(".bodyContainer").css({'height':bodyH-headerH+'px'});
        // var padding=parseInt($(".TabControl-contentContainer").css("padding"))*2;
        var padding=22;
        $(".TabControl-contentContainer").css({'width':bodyW-sideW-padding+'px','height':bodyH-headerH-padding+'px'});
        $(".Content-taskCon").css({'height':bodyH-headerH-padding-51+'px','width':bodyW-sideW-padding-20+'px'});
    }
    isLogin(server){
      var self=this;
      var loginData;
            $.ajax({
                type: "GET",
                async: false,
                url: server+"HelperManage/isLogin",
                success: function (data) {
                    loginData=self.initData(data,server);
                }
            });
    return loginData;
    }
    initData(data,server){
        if(data.status==-1){
            window.location.href="login.html";
        }else if(data.status==200){
            var userID=data.data.id;
            // var userID=5;
            return {userID:userID};
       }
    }
    render(){
        if(this.state.isLogin){
            return <div id="app">
                <WebHead name="后台管理系统" data={{outUrl:""}}/>
                <div className="bodyContainer">
                    <SideTab data={{helperData:this.state.helperData,userID:this.state.userID,url:this.state.server}}></SideTab>
                </div>
            </div>
        }
    }
}

ReactDOM.render(<App/>,document.getElementById("root"))
